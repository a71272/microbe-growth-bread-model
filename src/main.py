"""
===============================================================================
ENGR 13000 Fall 2021

Program Description
    This program computes a model of moldy bread.

Assignment Information
    Assignment:     Project 4
    Author:         Ari Fishkin, afishkin@purdue.edu
    Team ID:        LC1 - 03

Contributor:    Name, login@purdue [repeat for each]
    My contributor(s) helped me:
    [ ] understand the assignment expectations without
        telling me how they will approach it.
    [ ] understand different ways to think about a solution
        without helping me plan my solution.
    [ ] think through the meaning of a specific error or
        bug present in my code without looking at my code.
    Note that if you helped somebody else with their code, you
    have to list that person as a contributor here as well.
    
ACADEMIC INTEGRITY STATEMENT
I have not used source code obtained from any other unauthorized
source, either modified or unmodified. Neither have I provided
access to my code to another. The project I am submitting
is my own original work.
===============================================================================
"""
import draw
import json
import bread
import numpy as np
import itertools

# There are four different modes that this program can operate in:
# Nutrients mode – Lets you see the nutrients that you start with
# Air mode – Lets you see the distances from all of the holes 
# in the bread in a lot more detail
# Skip mode – Skips ahead to show you the end of the model evolution with a given amount of frames
# Video mode - Outputs a video showing a timelapse of the mold evolution
# There are three load modes:
# Export mode - exports the bread to a file and does nothing else
# Import mode - imports the bread from a file
# Memory mode - does not export or import and makes everything from scratch
# Export and import mode require a config parameter bread_file_name
def main():
    modes = ["nutrients", "air", "skip", "video"]
    load_modes = ["export", "import", "memory"]
    with open("config.json") as config_file:
        #Load config file
        config = json.load(config_file)
        #Make sure the mode and the load mode are valid.
        if config["mode"] not in modes:
            print("Invalid mode.")
            exit()
        elif config["load_mode"] not in load_modes:
            print("Invalid load mode.")
            exit()
        #Import if needed
        if config["load_mode"] == "import":
            b = bread.Bread.from_file(config["bread_file_name"])
        else:
            b = bread.Bread.from_config(config["x_size"],
                            config["y_size"],
                            config["z_size"],
                            config["num_holes"],
                            config["average_hole_radius"],
                            config["hole_standard_deviation"],
                            config["mold_seed_point"])
        #Switch between all of the modes.
        if config["load_mode"] == "export":
            b.export(config["bread_file_name"])
        elif config["mode"] == "nutrients":
            draw.draw_bread_nutrients(b.nutrients)
        elif config["mode"] == "air":
            draw.draw_bread_air(b.air)
        else:
            m = bread.MoldModel(b,
                                config["air_factor"],
                                config["nutrient_consumption"],
                                config["growth_factor"])
            if config["mode"] == "skip":
                for i in range(config["frames"]):
                    m.evolve()
                    #Every 60th frame, print that everything's working fine
                    if not i%60:
                        print(f"Frame: {i}")
                draw.draw_bread_mold(b.mold)
            elif config["mode"] == "video":
                if config["sinusoid"]:
                    angles = draw.sinusoidal_azimuth(config["cycle_length"], config["stop_frames"], config["sinusoid_start"])
                else:
                    angles = itertools.repeat(135)
                draw.evolve_model_plot(m, config["frames"], config["output_file_name"], angles, config["circle_stops"])


if __name__ == "__main__":
    main()