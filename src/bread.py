"""
===============================================================================
ENGR 13000 Fall 2021

Program Description
    This program computes a model of moldy bread.

Assignment Information
    Assignment:     Project 4
    Author:         Ari Fishkin, afishkin@purdue.edu
    Team ID:        LC1 - 03

Contributor:    Name, login@purdue [repeat for each]
    My contributor(s) helped me:
    [ ] understand the assignment expectations without
        telling me how they will approach it.
    [ ] understand different ways to think about a solution
        without helping me plan my solution.
    [ ] think through the meaning of a specific error or
        bug present in my code without looking at my code.
    Note that if you helped somebody else with their code, you
    have to list that person as a contributor here as well.
    
ACADEMIC INTEGRITY STATEMENT
I have not used source code obtained from any other unauthorized
source, either modified or unmodified. Neither have I provided
access to my code to another. The project I am submitting
is my own original work.
===============================================================================
"""
import numpy as np
from functools import cache
import math

#Makes circular holes
#Cached for performance
@cache
def make_hole(radius: int) -> np.ndarray:
    r = np.arange(-radius,radius+1)
    x,y,z = np.ix_(r,r,r)
    hole = (np.sqrt(np.square(x) + np.square(y) + np.square(z)) > radius).astype(int)
    hole.flags.writeable = False #For caching
    return hole

# Stores the bread on which the mold grows
class Bread:
    shape: tuple[int, int, int]
    nutrients: np.ndarray 
    air: np.ndarray
    mold: np.ndarray
    mold_seed: tuple[int, int, int]

    #Makes the bread, and then makes holes with the provided parameters
    #The air parameter is the distance of a given point from a hole, 
    #which helps determine growth
    @staticmethod
    def from_config(x_size: int, y_size: int, z_size: int, n_holes: int, avg_hole_rad: float, hole_std_dev: float, mold_seed: tuple[int, int, int]):
        shape = (x_size, y_size, z_size)
        nutrients = np.ones(shape)
        mold = np.zeros(shape)
        if avg_hole_rad*2 > x_size or avg_hole_rad*2 > y_size or avg_hole_rad*2 > z_size:
            print("Average hole diameter bigger than size of bread, will not form breadlike structure.")
            exit()
        if mold_seed[0] > x_size or mold_seed[1] > y_size or mold_seed[2] > z_size:
            print("Mold seed outside of bread.")
            exit()
        mold[mold_seed[0],mold_seed[1],mold_seed[2]] = 1
        #Minimization functions will always prefer new arrays when filling this way
        air = np.full(shape, np.inf)
        bread = Bread(shape, nutrients, air, mold, mold_seed)
        rng = np.random.Generator(np.random.PCG64(12345))
        hole_sizes = np.abs(np.round(rng.normal(avg_hole_rad, hole_std_dev, (n_holes)))).astype(int)
        hole_x_pos = rng.integers(0,x_size,n_holes)
        hole_y_pos = rng.integers(0,y_size,n_holes)
        hole_z_pos = rng.integers(0,z_size,n_holes)
        for i, (x, y, z, hole_rad) in enumerate(zip(hole_x_pos, hole_y_pos, hole_z_pos, hole_sizes)):
            hole = make_hole(hole_rad)
            hole_size = 2*hole_rad+1
            bread._add_hole(hole, x, y, z, hole_rad, hole_size)
            if not i%100:
                print(f"Hole: {i}")
        bread.air = np.sqrt(bread.air)
        return bread
    
    #Initialize the bread
    def __init__(self, shape: tuple[int, int, int], nutrients: np.ndarray, air: np.ndarray, mold: np.ndarray, mold_seed: tuple[int, int, int]):
        self.shape = shape
        self.nutrients = nutrients
        self.air = air
        self.mold = mold
        self.mold_seed = mold_seed
    
    #Make from file
    @staticmethod
    def from_file(file_name: str):
        with np.load(file_name) as data:
            bread = Bread(data["shape"], data["nutrients"], data["air"], np.zeros(data["shape"]), tuple(data["mold_seed"]))
            bread.shape = data["shape"]
            bread.nutrients = data["nutrients"]
            bread.air = data["air"]
            bread.mold_seed = tuple(data["mold_seed"])
            bread.mold[bread.mold_seed[0],bread.mold_seed[1],bread.mold_seed[2]] = 1
            return bread

    #Carves out a hole in the nutrients ndarray
    def _add_hole(self, hole: np.ndarray, x: int, y: int, z: int, hole_rad: int, hole_size: int):
        #Default indexing ranges that can be changed by hole chop off
        x_range = (x-hole_rad, x+hole_rad+1)
        y_range = (y-hole_rad, y+hole_rad+1)
        z_range = (z-hole_rad, z+hole_rad+1)

        x_size, y_size, z_size = self.shape

        #Make sure the hole can fit, if not chop it off
        if x < hole_size:
            x_range = (0, x)
            hole = hole[:x,...]
        elif x_size - x < hole_size:
            x_range = (x, x_size)
            hole = hole[:x_size - x,...]
        
        if y < hole_size:
            y_range = (0, y)
            hole = hole[:, :y, :]
        elif y_size - y < hole_size:
            y_range = (y, y_size)
            hole = hole[:, :y_size - y, :]

        if z < hole_size:
            z_range = (0, z)
            hole = hole[..., :z]
        elif z_size - z < hole_size:
            z_range = (z, z_size)
            hole = hole[..., :z_size - z]
        
        self.nutrients[x_range[0]:x_range[1],y_range[0]:y_range[1],z_range[0]:z_range[1]] = hole

        
        #Updates the air array with new distances from the hole
        x_air, y_air, z_air = np.ogrid[0:self.shape[0],0:self.shape[1],0:self.shape[2]]
        dists_from_hole = np.round(np.square(x_air - x) + np.square(y_air - y) + np.square(z_air - z)) - hole_rad*hole_rad
        dists_from_hole = np.maximum(dists_from_hole, 0)

        self.air = np.minimum(self.air, dists_from_hole)
    
    #Exports to a file
    def export(self, file_name: str):
        np.savez_compressed(file_name[:-4], shape=self.shape, nutrients=self.nutrients, air=self.air, mold_seed=self.mold_seed)

#A mold model which will be able to be evolved
#The evolve function moves the bread forward a timestep
#This model is based on a Laplacian model, where the Laplace of the mold affects growth
#which is the basis behind a lot of growth models
#However, a sqrt is taken here to avoid wacky overruns (even though it's not really proper)
#The growth factor defines the propensity for growth
#The bigger the growth factor, the faster the growth
#Nutrient depletion slows growth, and being farther from the air slows growth
#The air factor determines how much the distance from the air constrains growth
#The bigger the number, the more constrained
#Nutrient consumption determines how much one unit of mold consumes per timestep
class MoldModel:
    bread: Bread
    air_distance_factors: np.ndarray
    nutrient_consumption: float
    growth_factor: float
    shape: tuple[int,int,int]

    #Initializes the MoldModel with the parameters
    def __init__(self, bread: Bread, air_factor: float, nutrient_consumption: float, growth_factor: float):
        self.bread = bread
        self.shape = bread.shape
        self.air_distance_factors = (1-np.arctan(air_factor*bread.air)/(math.pi/2))
        self.nutrient_consumption = nutrient_consumption
        self.growth_factor = growth_factor
    

    def evolve(self):
        #Get the square root of the Laplacian (initial growth baseline)
        grad_x, grad_y, grad_z = np.gradient(self.bread.mold)
        grad_xx = np.gradient(grad_x, axis=0)
        grad_yy = np.gradient(grad_y, axis=1)
        grad_zz = np.gradient(grad_z, axis=2)
        grad_mag = np.sqrt(np.square(grad_xx) + np.square(grad_yy) + np.square(grad_zz))
        #Multiply by air distance factors (air constraint)
        grad_mag *= self.air_distance_factors
        #Multiply by nutrients (will always be less than or equal to 1, also applies holes)
        grad_mag *= self.bread.nutrients
        #Multiply by growth factor
        grad_mag *= self.growth_factor
        self.bread.mold += grad_mag
        self.bread.nutrients -= self.bread.mold*self.nutrient_consumption
        #Prevent negative nutrient values!
        self.bread.nutrients = np.maximum(self.bread.nutrients, 0)

        