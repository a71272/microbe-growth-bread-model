"""
===============================================================================
ENGR 13000 Fall 2021

Program Description
    This program computes a model of moldy bread.

Assignment Information
    Assignment:     Project 4
    Author:         Ari Fishkin, afishkin@purdue.edu
    Team ID:        LC1 - 03

Contributor:    Name, login@purdue [repeat for each]
    My contributor(s) helped me:
    [ ] understand the assignment expectations without
        telling me how they will approach it.
    [ ] understand different ways to think about a solution
        without helping me plan my solution.
    [ ] think through the meaning of a specific error or
        bug present in my code without looking at my code.
    Note that if you helped somebody else with their code, you
    have to list that person as a contributor here as well.
    
ACADEMIC INTEGRITY STATEMENT
I have not used source code obtained from any other unauthorized
source, either modified or unmodified. Neither have I provided
access to my code to another. The project I am submitting
is my own original work.
===============================================================================
"""
import pyvista
import numpy as np
import math
import bread
from typing import Iterable, Generator

#Draws the nutrients of the bread in a way to make the structure of the bread easy to see.
def draw_bread_nutrients(arr: np.ndarray):
    plotter = pyvista.Plotter()
    grid = pyvista.UniformGrid()
    grid.dimensions = np.array(arr.shape) + 1
    grid.cell_data["Nutrients"] = arr.flatten(order="F")
    plotter.set_background("black")
    plotter.add_volume(grid,cmap="Greys",opacity_unit_distance=0.6,preference="cells",show_scalar_bar=False)
    plotter.camera_position = 'yz'
    plotter.camera.elevation = 40
    plotter.show()

#Draws the distances from air pockets in the bread in a way to make these distances easy to see. 
def draw_bread_air(arr: np.ndarray):
    plotter = pyvista.Plotter()
    grid = pyvista.UniformGrid()
    grid.dimensions = np.array(arr.shape) + 1
    grid.cell_data["Air^2"] = np.square(arr).flatten(order="F")
    plotter.add_volume(grid,cmap="viridis",preference="cells")
    plotter.camera_position = 'yz'
    plotter.camera.elevation = 40
    plotter.show()

#Draws a frame from the mold model. 
def draw_bread_mold(arr: np.ndarray):
    plotter = pyvista.Plotter()
    grid = pyvista.UniformGrid()
    grid.dimensions = np.array(arr.shape) + 1
    grid.cell_data["Mold"] = arr.flatten(order="F")
    plotter.add_volume(grid,cmap="viridis",preference="cells")
    plotter.camera_position = 'yz'
    plotter.camera.elevation = 40
    plotter.show()

#Generates a nice sinusoidal azimuth curve so that the viewer doesn't get whiplash
def sinusoidal_azimuth(cycle_length: int, stop_frames: int, offset: int) -> Generator[float]:
    for i in range(offset):
        yield 135
    i = 0
    while True:
        while i < cycle_length/2:
            yield 45*math.cos(2*math.pi*i/cycle_length)+90
            i += 1
        for j in range(stop_frames):
            yield 45
        while i < cycle_length:
            yield 45*math.cos(2*math.pi*i/cycle_length)+90
            i += 1
        for j in range(stop_frames-1):
            yield 135
        i = 0

#Evolve the model and plot the results in a video
#The angles should be an infinite generator
#If circle_stops is not 0, it does a spin around to give a better visual, stopping x times along the circle
def evolve_model_plot(model: bread.MoldModel, frames: int, output_file_name: str, angles: Iterable[int], circle_stops: int = 0):
    plotter = pyvista.Plotter(off_screen=True)
    grid = pyvista.UniformGrid()
    grid.dimensions = np.array(model.shape) + 1
    plotter.open_movie(output_file_name)
    plotter.show(auto_close=False)
    #Evolve the model and plot each frame
    for i, angle in zip(range(frames), angles):
        model.evolve()
        grid.cell_data["Mold"] = model.bread.mold.flatten(order="F")
        grid_actor = plotter.add_volume(grid,cmap="viridis",preference="cells")
        plotter.camera_position = 'yz'
        plotter.camera.elevation = 40
        plotter.camera.azimuth = angle
        plotter.write_frame()
        plotter.remove_actor(grid_actor)
        if not i%60:
            print(f"Frame: {i}")
    #Calculate the ending animation
    if circle_stops > 0:
        grid.cell_data["Mold"] = model.bread.mold.flatten(order="F")
        grid_actor = plotter.add_volume(grid,cmap="viridis",preference="cells")
        plotter.camera_position = 'yz'
        plotter.camera.elevation = 40
        for i in range(circle_stops):
            plotter.camera.azimuth = i*360/circle_stops + angle
            plotter.write_frame()
    plotter.close()