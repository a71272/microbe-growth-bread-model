# MicrobeGrowthBreadModel

Models the growth of microbes on objects with air pockets, such as breads using a Laplacian-based model. Built in Python using the Pyvista library. Originally made for the ENGR 130 class at Purdue University.

## Installation
Make sure that you have Python 3 and git installed and on your PATH (Info about PATH – [Windows](https://web.archive.org/web/20211106094738/https://www.maketecheasier.com/what-is-the-windows-path/)/[macOS](https://web.archive.org/web/20211116062307/https://wpbeaches.com/how-to-add-to-the-shell-path-in-macos-using-terminal/)/[Linux](https://web.archive.org/web/20211125090704/https://opensource.com/article/17/6/set-path-linux)).

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Project status
This project is archived, but may be restarted to add more detail to the model.

